FROM nginx:alpine
COPY index.html /usr/share/nginx/html/index.html
COPY aboutme.html /usr/share/nginx/html/aboutme.html
COPY memories.html /usr/share/nginx/html/memories.html
COPY hobbies.html /usr/share/nginx/html/hobbies.html
COPY styles /usr/share/nginx/html/styles
COPY images /usr/share/nginx/html/images
RUN chmod 755 -R /usr/share/nginx/html/styles
RUN chmod 755 -R /usr/share/nginx/html/images
